# presentations

These are presentations I've built or forked.

* _How Humans See Data_ is forked from John Rauser's excellent presentation of the same name ([VelocityConf 2016 presentation](https://www.youtube.com/watch?v=fSgEeI2Xpdc); [GitHub source](https://github.com/jrauser/writing/tree/master/how_humans_see_data))
* The as-of-yet unnamed _Data Analysis_ presentation is based on a [blog post](https://kbroman.org/blog/2019/08/15/data-analysis-principles/) by Karl Broman
