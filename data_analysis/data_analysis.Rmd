---
title: "Data Analysis"
author: "Daniel Spracklin"
date: "`r format(Sys.Date(), '%B %e, %Y')`"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: solarized_light
      highlightLines: true
      countIncrementalSlides: false
---

class: inverse, left, middle

```{r setup, include = F, fig.showtext = T}
knitr::opts_chunk$set(echo = F, warning = F, message = F, fig.retina = 3)
library(tidyverse)
library(readr)
library(stringr)
library(scales)
library(broom)
library(viridis)
library(xaringanthemer)
library(correlation)

style_solarized_light()

theme_light <- function(base_size = 14) {

  checkmate::assert_number(base_size)
  
  large_size <- base_size * 16 / 14
  small_size <- base_size * 12 / 14
  tiny_size <- base_size * 11 / 14
  
  xaringanthemer::theme_xaringan(background_color = "#fdf6e3") + # needs American spelling
    ggplot2::theme(plot.title = ggplot2::element_text(face = "bold", size = large_size),
                   plot.subtitle = ggplot2::element_text(size = small_size),
                   plot.caption = ggplot2::element_text(size = tiny_size),
                   axis.text = ggplot2::element_text(size = small_size),
                   legend.title = element_text(size = base_size),
                   legend.text = ggplot2::element_text(size = base_size),
                   strip.text = ggplot2::element_text(size = base_size),
                   strip.background = element_blank(),
                   axis.ticks = ggplot2::element_blank())
}

theme_set(theme_light(14))
```

# Data analysis

- What's the question
- Where does the data come from
- Make a graph
- Is the result intuitive or too good to be true?
- How could things have gone wrong? How would you know?
- Look at outliers
- Make a standard error
- Try taking logs or differences
- Use position on a common scale
- Avoid normality assumptions
- Remember Simpson's Paradox

---
class: inverse, center, middle

# What's the question?

---
class: inverse, center, middle

# Where does the data come from?


---
class: inverse, center, middle

# Make a graph

---
class: inverse, center, middle

# Is the result too good to be true?

---
class: inverse, center, middle

# How could things have gone wrong?

---
class: inverse, center, middle

# Look at outliers

---
class: inverse, center, middle

# Make a standard error

---
class: inverse, center, middle

# Try taking logs or differences

---
class: inverse, center, middle

# Use position on a common scale

---
class: inverse, center, middle

# Avoid normality assumptions

---
class: inverse, center, middle

# Remember Simpson's paradox

---
class: center, middle

```{r}
set.seed(1)
simpson <- simulate_simpson(n = 100, groups = 5, r = 0.75)

simpson %>% 
  ggplot(aes(V1, V2)) +
  geom_point()
```

---
class: center, middle

The relationship looks clear

```{r}
simpson %>% 
  ggplot(aes(V1, V2)) +
  geom_point() +
  geom_smooth(method = "lm", se = F, colour = "black")
```

---
class: center, middle

But subgroups have the opposite relationship

```{r}
simpson %>% 
  ggplot(aes(V1, V2, colour = Group)) +
  geom_point() +
  geom_smooth(method = "lm", se = F) +
  scale_colour_viridis_d(name = "Group") +
  theme(legend.position = "bottom")
```
